package aloadecuado.com.co.chatsockettcp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.textEditText)
    EditText textEditText;

    @BindView(R.id.sendButton)
    Button sendButton;

    //Socket

    private static Socket s;
    //private static ServerSocket ss;
    private static InputStreamReader isr;
    private static BufferedReader br;
    private static PrintWriter printWriter;

    String messagge = "";

    private String ip = "192.168.0.20";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.sendButton) void sendMesssagge(View view){

        messagge = textEditText.getText().toString();
        MyTask mt = new MyTask();
        mt.execute();

        Toast.makeText(this, "send data", Toast.LENGTH_SHORT).show();
    }

    class MyTask extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {

            try{
                s = new Socket(ip, 5000);
                printWriter = new PrintWriter(s.getOutputStream());
                printWriter.write(messagge);
                printWriter.flush();
                printWriter.close();
                s.close();
            }catch (Exception e){
                Toast.makeText(MainActivity.this, "error: " + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }

            return null;
        }
    }
}
