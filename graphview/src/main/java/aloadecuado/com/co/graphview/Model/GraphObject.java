package aloadecuado.com.co.graphview.Model;

public class GraphObject {

    private int tag = 0;
    private int colorLine = 0;
    private int widthLine = 0;
    private int offSet = 0;

    public int getTag() {
        return tag;
    }

    public void setTag(int tag) {
        this.tag = tag;
    }

    public int getColorLine() {
        return colorLine;
    }

    public void setColorLine(int colorLine) {
        this.colorLine = colorLine;
    }

    public int getWidthLine() {
        return widthLine;
    }

    public void setWidthLine(int widthLine) {
        this.widthLine = widthLine;
    }

    public int getOffSet() {
        return offSet;
    }

    public void setOffSet(int offSet) {
        this.offSet = offSet;
    }

    public GraphObject(int tag, int colorLine, int widthLine, int offSet) {
        this.tag = tag;
        this.colorLine = colorLine;
        this.widthLine = widthLine;
        this.offSet = offSet;
    }

    public GraphObject() {

    }
}
