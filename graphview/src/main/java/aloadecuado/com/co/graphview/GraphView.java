package aloadecuado.com.co.graphview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ScrollView;

import java.util.ArrayList;
import java.util.List;

import aloadecuado.com.co.graphview.Model.*;

public class GraphView extends View {

    private Bitmap  mBitmap;
    private Paint   mPaint = new Paint();
    private Paint   mPaint1 = new Paint();
    private Paint   mPaint2 = new Paint();
    private Canvas  mCanvas = new Canvas();

    private float   mSpeed = 1.0f;
    private float   mLastX;
    private float   mLastX1;
    private float   mLastX2;

    private float   mScale;

    private float   mLastValue;
    private float   mLastValue1;
    private float   mLastValue2;


    private float   mYOffset;
    private int     mColor;
    private int     mColor1;
    private int     mColor2;
    private float   mWidth;
    private float   maxValue = 1024f;

    public GraphView(Context context) {
        super(context);
        init();
    }

    public GraphView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init(){
        mColor = Color.argb(192, 64, 128, 64);
        mColor1 = Color.argb(192, 0, 255, 0);
        mColor2 = Color.argb(192, 0, 0, 255);
        mPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        mPaint1.setFlags(Paint.ANTI_ALIAS_FLAG);
        mPaint1.setFlags(Paint.ANTI_ALIAS_FLAG);
    }

    public void addDataPointx(float value){
        final Paint paint = mPaint;
        float newX = mLastX + mSpeed;
        final float v = mYOffset + value * mScale;

        paint.setColor(mColor);
        mCanvas.drawLine(mLastX, mLastValue - 300, newX, v - 300, paint);
        mLastValue = v;
        mLastX += mSpeed;

        invalidate();
    }

    public void addDataPointy(float value){
        final Paint paint = mPaint1;
        float newX = mLastX1 + mSpeed;
        final float v = mYOffset + value * mScale;

        paint.setColor(mColor1);
        mCanvas.drawLine(mLastX1, mLastValue1 - 300, newX, v - 300, paint);
        mLastValue1 = v;
        mLastX1 += mSpeed;

        invalidate();
    }

    public void addDataPointz(float value){
        final Paint paint = mPaint2;
        float newX = mLastX2 + mSpeed;
        final float v = mYOffset + value * mScale;

        paint.setColor(mColor2);
        mCanvas.drawLine(mLastX2, mLastValue2 - 300, newX, v - 300, paint);
        mLastValue2 = v;
        mLastX2 += mSpeed;

        invalidate();
    }

    public void setMaxValue(int max){
        maxValue = max;
        mScale = - (mYOffset * (1.0f / maxValue));
    }

    public void setSpeed(float speed){
        mSpeed = speed;
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.RGB_565);
        mCanvas.setBitmap(mBitmap);
        mCanvas.drawColor(0xFFFFFFFF);
        mYOffset = h;
        mScale = - (mYOffset * (1.0f / maxValue));
        mWidth = w;
        mLastX = mWidth;
        super.onSizeChanged(w, h, oldw, oldh);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        synchronized (this) {
            if (mBitmap != null) {
                if (mLastX >= mWidth) {
                    mLastX = 0;
                    mLastX1 = 0;
                    mLastX2 = 0;

                    final Canvas cavas = mCanvas;
                    cavas.drawColor(0xFFFFFFFF);
                    mPaint.setColor(0xFF777777);

                    cavas.drawLine(0, mYOffset, mWidth, mYOffset, mPaint);
                }
                canvas.drawBitmap(mBitmap, 0, 0, null);
            }
        }
    }
    //Paint paint = new Paint();
    /*Canvas canvasPaint;
    List<Paint> paintList;

    //List<Canvas> canvasList;

    List<GraphObject> graphObjectList;
    //private int graphColor = Color.rgb(255, 0, 0);
    int[] afterPoints;

    int countPoints = 1;

    private Bitmap mBitmap;
    private Paint   mPaint = new Paint();
    private Canvas  mCanvas = new Canvas();

    private float   mSpeed = 1.0f;
    private float   mLastX;
    private float   mScale;
    private float   mLastValue;
    private float   mYOffset;
    private int     mColor;
    private float   mWidth;
    private float   maxValue = 1024f;

    public GraphView(Context context) {
        super(context);
    }

    public GraphView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GraphView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public GraphView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }


    public void setGraphs(List<GraphObject> graphObjectList){
        this.graphObjectList = graphObjectList;

        paintList = new ArrayList<>();

        afterPoints = new int[graphObjectList.size()];
        for (GraphObject graphObject : this.graphObjectList){
            Paint paint = new Paint();
            paint.setColor(graphObject.getColorLine());

            paint.setStrokeWidth(graphObject.getWidthLine()); // ancho del pincel
            paint.setStyle(Paint.Style.STROKE);

            paintList.add(paint);

        }
    }
    @Override
    public void onDraw(Canvas canvas) {

        synchronized (this) {
            if (mBitmap != null) {
                if (mLastX >= mWidth) {
                    mLastX = 0;
                    final Canvas cavas = mCanvas;
                    cavas.drawColor(0xFFFFFFFF);
                    mPaint.setColor(0xFF777777);
                    cavas.drawLine(0, mYOffset, mWidth, mYOffset, mPaint);
                }
                canvas.drawBitmap(mBitmap, 0, 0, null);
            }
        }
        canvasPaint = canvas;
       /* paint.setColor(graphColor);

        paint.setStrokeWidth(2); // ancho del pincel
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawLine(0, 0, 200, 200, paint);
        canvas.drawLine(20, 0, 0, 20, paint);*/
        //canvas.drawColor(graphColor);



    /*}

    public void setInitValues(){
        this.graphObjectList = new ArrayList<>();

        paintList = new ArrayList<>();

        afterPoints = new int[3];
    }
    public void addGraph(GraphObject graphObject){

        this.graphObjectList.add(graphObject);


            Paint paint = new Paint();
            paint.setColor(graphObject.getColorLine());

            paint.setStrokeWidth(graphObject.getWidthLine()); // ancho del pincel
            paint.setStyle(Paint.Style.STROKE);

            paintList.add(paint);






    }

    public void setPointGraph(int tag, int point){

        //if afterPoints[tag] != null{
        if (canvasPaint != null) {

            Paint paint1 = new Paint();
            paint1.setColor(Color.rgb(255, 0, 0));
            paint1.setStrokeWidth(2); // ancho del pincel
            paint1.setStyle(Paint.Style.STROKE);

            canvasPaint.drawLine(0, 0, 300, 300, paint1);
            //mCanvas.drawColor(Color.rgb(255, 0, 0));
            afterPoints[tag] = point;
            countPoints++;
        }
        //}



    }

    public void addDataPoint(float value){
        final Paint paint = mPaint;
        float newX = mLastX + mSpeed;
        final float v = mYOffset + value * mScale;

        paint.setColor(mColor);
        mCanvas.drawLine(mLastX, mLastValue, newX, v, paint);
        mLastValue = v;
        mLastX += mSpeed;

        invalidate();
    }*/
}
