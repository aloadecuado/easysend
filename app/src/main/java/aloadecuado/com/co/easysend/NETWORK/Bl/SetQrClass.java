package aloadecuado.com.co.easysend.NETWORK.Bl;


import android.content.Context;

import com.google.firebase.database.DataSnapshot;

import java.text.SimpleDateFormat;
import java.util.Date;

import aloadecuado.com.co.easysend.MODELS.OrderLinked;
import aloadecuado.com.co.easysend.MODELS.UserObject;
import aloadecuado.com.co.easysend.NETWORK.Ws.SetObjectClass;
import aloadecuado.com.co.easysend.R;
import aloadecuado.com.co.easysend.UTIL.AlertsClass;

import static aloadecuado.com.co.easysend.UTIL.GlobalConstansAndVarClass.FIREURL_ORDERS_LINKED;

public class SetQrClass {
    public interface OnGetOrderLinked{
        void onGetLinked(OrderLinked orderLinked);
    }

    public void setLinkOrder(Context context, String textScan, UserObject userObject, final OnGetOrderLinked listener){

        OrderLinked orderLinked = new OrderLinked();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        Date convertedDate = new Date();
        String strDate = dateFormat.format(convertedDate);
        orderLinked.setDateRegister(strDate);

        String Uiid = textScan.split("@@@")[0];
        String UiidId = textScan.split("@@@")[1];
        orderLinked.setIdSession(Uiid);
        orderLinked.setId(UiidId);
        orderLinked.setUserObject(userObject);
        new SetObjectClass().SetObjectKeyForCustomKey(orderLinked, FIREURL_ORDERS_LINKED, UiidId, new SetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot) {
                OrderLinked orderLinked1 = snapshot.getValue(OrderLinked.class);

                if(orderLinked1 != null)
                {
                    listener.onGetLinked(orderLinked1);
                }
                else
                {
                    new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), context.getResources().getString(R.string.alert_message_no_user));
                }
            }

            @Override
            public void onGetError(String Err) {
                new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), context.getResources().getString(R.string.alert_message_no_user));
            }
        });



    }
}
