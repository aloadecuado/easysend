package aloadecuado.com.co.easysend.MODELS;

import java.util.List;

public class OrderLinked {
    private String id = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String idSession = "";

    private UserObject userObject;
    private String dateRegister = "";
    private List<String> urlsDocuments = null;

    public String getIdSession() {
        return idSession;
    }

    public void setIdSession(String idSession) {
        this.idSession = idSession;
    }

    public UserObject getUserObject() {
        return userObject;
    }

    public void setUserObject(UserObject userObject) {
        this.userObject = userObject;
    }

    public String getDateRegister() {
        return dateRegister;
    }

    public void setDateRegister(String dateRegister) {
        this.dateRegister = dateRegister;
    }

    public List<String> getUrlsDocuments() {
        return urlsDocuments;
    }

    public void setUrlsDocuments(List<String> urlsDocuments) {
        this.urlsDocuments = urlsDocuments;
    }
}
