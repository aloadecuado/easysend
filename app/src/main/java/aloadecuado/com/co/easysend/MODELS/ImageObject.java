package aloadecuado.com.co.easysend.MODELS;

public class ImageObject {

    private String id = "";
    private String urlImageReference = "";
    private String dateRegister = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrlImageReference() {
        return urlImageReference;
    }

    public void setUrlImageReference(String urlImageReference) {
        this.urlImageReference = urlImageReference;
    }

    public String getDateRegister() {
        return dateRegister;
    }

    public void setDateRegister(String dateRegister) {
        this.dateRegister = dateRegister;
    }
}
