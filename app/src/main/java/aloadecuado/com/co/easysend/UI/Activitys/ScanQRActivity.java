package aloadecuado.com.co.easysend.UI.Activitys;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.solver.widgets.Analyzer;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.PointF;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.IOException;

import aloadecuado.com.co.easysend.MODELS.OrderLinked;
import aloadecuado.com.co.easysend.NETWORK.Bl.SetQrClass;
import aloadecuado.com.co.easysend.R;

import static aloadecuado.com.co.easysend.UTIL.GlobalConstansAndVarClass.ORDERLINKED;
import static aloadecuado.com.co.easysend.UTIL.GlobalConstansAndVarClass.USEROBJECT;
import static java.security.AccessController.getContext;

public class ScanQRActivity extends AppCompatActivity implements QRCodeReaderView.OnQRCodeReadListener {

    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 2020;
    private QRCodeReaderView qrCodeReaderView;

    private static final String TAG = "MainActivity";

    private AdView mAdView;
    SurfaceView cameraView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_qr);

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        qrCodeReaderView = (QRCodeReaderView) findViewById(R.id.qrdecoderview);
        qrCodeReaderView.setOnQRCodeReadListener(this);

        // Use this function to enable/disable decoding
        qrCodeReaderView.setQRDecodingEnabled(true);

        // Use this function to change the autofocus interval (default is 5 secs)
        qrCodeReaderView.setAutofocusInterval(2000L);

        // Use this function to enable/disable Torch
        qrCodeReaderView.setTorchEnabled(true);

        // Use this function to set front camera preview
        qrCodeReaderView.setFrontCamera();

        // Use this function to set back camera preview
        qrCodeReaderView.setBackCamera();



    }

    @Override
    protected void onResume() {
        super.onResume();
        qrCodeReaderView.startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        qrCodeReaderView.stopCamera();
    }


    @Override
    public void onQRCodeRead(String text, PointF[] points) {


        new SetQrClass().setLinkOrder(this, text, USEROBJECT, new SetQrClass.OnGetOrderLinked() {
            @Override
            public void onGetLinked(OrderLinked orderLinked) {
                ORDERLINKED = orderLinked;
                ScanQRActivity.this.finish();

            }
        });
    }
}
