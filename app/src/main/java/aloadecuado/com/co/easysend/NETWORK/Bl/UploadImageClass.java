package aloadecuado.com.co.easysend.NETWORK.Bl;

import android.graphics.Bitmap;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;

import aloadecuado.com.co.easysend.MODELS.UserObject;
import aloadecuado.com.co.easysend.NETWORK.Ws.UploadFileClass;


/**
 * Created by pedrodaza on 24/01/18.
 */

public class UploadImageClass {

    public interface OnSetDataImage
    {
        void OnSetImage(String UrlDowload);
    }


    public void  UploadBitmapUser(UserObject userObject, Bitmap bitmap, final OnSetDataImage listener)
    {

        String Nombre = "";
        String Referencia = "";

        if(userObject == null){
            return;
        }
        if (userObject.getUrlsDocuments() == null)
        {
            Nombre = userObject.getId() + "Documets" + userObject.getIdNumber() + "Id" + 0 + ".jpg";
            Referencia = userObject.getId() + "/" + "Documets" + "/" + userObject.getIdNumber() +".jpg";

        }
        else
        {
            Nombre = userObject.getId() + "Documets" + userObject.getIdNumber() + "Id" + userObject.getUrlsDocuments().size() + ".jpg";
            Referencia = userObject.getId() + "/" + "Documets" + "/" + userObject.getIdNumber() +".jpg";
        }



        Bitmap bmap = bitmap;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();
        new UploadFileClass().UploadFile(Referencia, Nombre, data, new UploadFileClass.OnSetDataFile() {
            @Override
            public void OnSetFile(String UrlDownload) {
                listener.OnSetImage(UrlDownload);
            }

            @Override
            public void OnSetError(String error) {

            }
        });
    }



}
