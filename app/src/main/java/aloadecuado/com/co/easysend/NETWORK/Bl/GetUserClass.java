package aloadecuado.com.co.easysend.NETWORK.Bl;

import android.content.Context;

import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.List;

import aloadecuado.com.co.easysend.MODELS.UserObject;
import aloadecuado.com.co.easysend.NETWORK.Ws.GetObjectClass;
import aloadecuado.com.co.easysend.R;
import aloadecuado.com.co.easysend.UTIL.AlertsClass;

import static aloadecuado.com.co.easysend.UTIL.GlobalConstansAndVarClass.FIREURL_USERS;


/**
 * Created by Pedro on 25/01/2018.
 */

public class GetUserClass {

    public interface onGetUsers
    {
        void onGetListUsers(List<UserObject> userObjects);
    }
    public interface onGetUser
    {
        void onGetUser(UserObject userObjects);
    }


    public void getUserForId(final Context context, String userId, final onGetUser listener)
    {
        new GetObjectClass().getSimpleObjectsForKey(FIREURL_USERS, userId, new GetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot, String key) {
                UserObject userObject = snapshot.getValue(UserObject.class);

                if(userObject != null)
                {
                    listener.onGetUser(userObject);
                }
                else
                {
                    new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), context.getResources().getString(R.string.alert_message_no_user));
                }
            }

            @Override
            public void onGetNullObject() {
                new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), context.getResources().getString(R.string.alert_message_no_user));
            }

            @Override
            public void OnGetError(String Err) {
                new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), Err);
            }
        });

    }

}
