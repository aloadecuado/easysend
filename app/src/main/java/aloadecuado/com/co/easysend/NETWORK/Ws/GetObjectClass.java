package aloadecuado.com.co.easysend.NETWORK.Ws;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * Created by pedrodaza on 28/12/17.
 */

public class GetObjectClass {

    public interface onGetObject
    {
        void onGetObject(DataSnapshot snapshot, String key);
        void onGetNullObject();
        void OnGetError(String Err);
    }

    public interface onGetListObject
    {
        void onGetLisObject(Iterable<DataSnapshot> snapshots, String key);
        void onGetNullObject();
        void OnGetError(String Err);
    }
    FirebaseDatabase DataBase;
    public  GetObjectClass()
    {
        DataBase = FirebaseDatabase.getInstance();
    }

    public void getSimpleListObjectUidKeyWhithEqualsToString(String Child, String Atrr, String equalsTo, final onGetObject listener)
    {
        DatabaseReference myRef = DataBase.getReference(Child);

        myRef.orderByChild(Atrr).equalTo(equalsTo).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.getValue() != null){
                    listener.onGetObject(dataSnapshot, dataSnapshot.getKey());
                }
                else
                {
                    listener.onGetNullObject();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                listener.OnGetError(databaseError.getMessage());
            }
        });
    }


    public void getSimpleListObjectUidKey(String Child, final onGetObject listener)
    {
        DatabaseReference myRef = DataBase.getReference(Child);

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.getValue() != null){
                    listener.onGetObject(dataSnapshot, dataSnapshot.getKey());
                }
                else
                {
                    listener.onGetNullObject();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                listener.OnGetError(databaseError.getMessage());
            }
        });
    }


    public void getSimpleListObjectUidKeyWhithEqualsTotwoString(String Child, String Atrr1, String equalsTo1, String Atrr2, String equalsTo2, final onGetObject listener)
    {
        DatabaseReference myRef = DataBase.getReference(Child);

        myRef.orderByChild(Atrr1).equalTo(equalsTo1).orderByChild(Atrr2) .equalTo(equalsTo2).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.getValue() != null){
                    listener.onGetObject(dataSnapshot, dataSnapshot.getKey());
                }
                else
                {
                    listener.onGetNullObject();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                listener.OnGetError(databaseError.getMessage());
            }
        });
    }

    public void getListenerListObjectUidKey(String Child, final onGetObject listener)
    {
        DatabaseReference myRef = DataBase.getReference(Child);

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.getValue() != null){
                    listener.onGetObject(dataSnapshot, dataSnapshot.getKey());
                }
                else
                {
                    listener.onGetNullObject();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                listener.OnGetError(databaseError.getMessage());
            }
        });
    }

    public void getListenerListObjectEqualsString(String Child, String atrr, String equalsto, final onGetObject listener)
    {
        DatabaseReference myRef = DataBase.getReference(Child);

        myRef.orderByChild(atrr).equalTo(equalsto).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.getValue() != null){
                    listener.onGetObject(dataSnapshot, dataSnapshot.getKey());
                }
                else
                {
                    listener.onGetNullObject();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                listener.OnGetError(databaseError.getMessage());
            }
        });
    }
    public void getListenereListObjectKeyWhithEqualsToString(String Child, String Atrr, String equalsTo, final onGetListObject listener)
    {
        DatabaseReference myRef = DataBase.getReference(Child);

        myRef.orderByChild(Atrr).equalTo(equalsTo).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null){


                    ArrayList<Object> audioRecordObjects = new ArrayList<>();
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren())
                    {
                        audioRecordObjects.add(dataSnapshot1.getValue(Object.class));
                    }

                    if (audioRecordObjects.size() >= 1)
                    {
                        listener.onGetLisObject(dataSnapshot.getChildren(), dataSnapshot.getKey());
                    }
                    else
                    {
                        listener.onGetNullObject();
                    }


                }
                else
                {
                    listener.onGetNullObject();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                listener.OnGetError(databaseError.getMessage());
            }
        });
    }

    public void getSimpleObjectsforAllChield(String Child, final onGetObject listener)
    {

        DatabaseReference myRef = DataBase.getReference(Child);

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.getValue() != null){
                    listener.onGetObject(dataSnapshot, dataSnapshot.getKey());
                }
                else
                {
                    listener.onGetNullObject();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                listener.OnGetError(databaseError.getMessage());
            }
        });
    }
    private static final String TAG = "GetObjectClass";
    public void getSimpleObjectsForKey(String Child, String key, final onGetObject listener)
    {

        DatabaseReference myRef = DataBase.getReference(Child).child(key);
        Log.v(TAG, "Ref firebase getSimpleObjectsForKey = " + myRef);
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.getValue() != null){
                    listener.onGetObject(dataSnapshot, dataSnapshot.getKey());
                }
                else
                {
                    listener.onGetNullObject();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                listener.OnGetError(databaseError.getMessage());
            }
        });
    }

    public void getListenerObjectForId(String Chield, String key, final onGetObject listener)
    {
        DatabaseReference myRef = DataBase.getReference(Chield).child(key);
        Log.v(TAG, "Ref firebase getSimpleObjectsForKey = " + myRef);
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null){
                    Log.v(TAG, "getListenerObjectForId dataSnapshot.getKey() = " + dataSnapshot.getKey());
                    listener.onGetObject(dataSnapshot, dataSnapshot.getKey());
                }
                else
                {
                    Log.v(TAG, "onGetNullObject()");
                    listener.onGetNullObject();

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                listener.OnGetError(databaseError.getMessage());
            }
        });
    }
}

