package aloadecuado.com.co.easysend.NETWORK.Bl;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.List;

import aloadecuado.com.co.easysend.MODELS.UserObject;
import aloadecuado.com.co.easysend.NETWORK.Ws.GetObjectClass;
import aloadecuado.com.co.easysend.NETWORK.Ws.SetObjectClass;
import aloadecuado.com.co.easysend.UTIL.AlertsClass;

import static aloadecuado.com.co.easysend.UTIL.GlobalConstansAndVarClass.FIREURL_USERS;



/**
 * Created by pedrodaza on 16/01/18.
 */

public class SetUserClass {

    public interface onGetUser
    {
        void onGetUser(UserObject userObject);
        void onGetUrl(String url);
    }


    public void createUserKeyForUid(final Context context, final UserObject userObject, final onGetUser listener)
    {

        new GetObjectClass().getSimpleListObjectUidKeyWhithEqualsToString(FIREURL_USERS, "uid", userObject.getUid(), new GetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot, String key) {


                for (DataSnapshot snapshot1 : snapshot.getChildren())
                {
                    UserObject userObject1 = snapshot1.getValue(UserObject.class);
                    userObject1.setId(snapshot1.getKey());
                    listener.onGetUser(userObject1);
                    break;
                }

            }

            @Override
            public void onGetNullObject() {
                new SetObjectClass().SetObjectKeyForAutoKey(userObject, FIREURL_USERS, new SetObjectClass.onGetObject() {
                    @Override
                    public void onGetObject(DataSnapshot snapshot) {
                        UserObject userObject1 = snapshot.getValue(UserObject.class);

                        userObject1.setId(snapshot.getKey());
                        listener.onGetUser(userObject1);
                    }

                    @Override
                    public void onGetError(String Err) {
                        new AlertsClass().showAlerInfo(context, "Información", Err);

                    }
                });
            }

            @Override
            public void OnGetError(String Err) {
                new AlertsClass().showAlerInfo(context, "Información", Err);


            }
        });

    }

    public  void addBitmapUserForId(Bitmap bitmap,final UserObject userObject, final onGetUser listener)
    {
        new UploadImageClass().UploadBitmapUser(userObject, bitmap, new UploadImageClass.OnSetDataImage() {
            @Override
            public void OnSetImage(String UrlDowload) {

                listener.onGetUrl(UrlDowload);
                List<String> strings = userObject.getUrlsDocuments();

                if(strings == null)
                {
                    strings = new ArrayList<>();
                    strings.add(UrlDowload);
                }
                else
                {
                    strings.add(UrlDowload);
                }

                userObject.setUrlsDocuments(strings);

                new SetObjectClass().SetObjectKeyForCustomKey(userObject, FIREURL_USERS, userObject.getId(), new SetObjectClass.onGetObject() {
                    @Override
                    public void onGetObject(DataSnapshot snapshot) {

                        UserObject userObject1 = snapshot.getValue(UserObject.class);
                        userObject1.setId(snapshot.getKey());
                        listener.onGetUser(userObject1);


                    }

                    @Override
                    public void onGetError(String Err) {

                    }
                });

            }
        });
    }


}
