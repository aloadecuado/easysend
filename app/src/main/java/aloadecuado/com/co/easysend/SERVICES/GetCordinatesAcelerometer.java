package aloadecuado.com.co.easysend.SERVICES;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.widget.Toast;

import aloadecuado.com.co.easysend.UI.Activitys.SendDataActivity;

import static aloadecuado.com.co.easysend.UI.Activitys.MainActivity.ONGETPOINTS;

public class GetCordinatesAcelerometer extends Service implements SensorEventListener {
    private SensorManager mSensorManager;
    static int count=0;
    //private ShakeEventListener mSensorListener;
    /** OnShakeListener that is called when shake is detected. */



    public interface OnGetPoints{
        void onGetPoitns(double x, double y, double z);
        void onGetIsMovet();
    }

    OnGetPoints onGetPoints;
    public GetCordinatesAcelerometer() {

        this.onGetPoints = ONGETPOINTS;
    }

    double[] pointsPositive = new double[3];
    double[] pointsNegative = new double[3];

    int countPointsPositive = 0;
    int countPointsNegative = 0;

    Boolean isPositive = false;
    Boolean isNegative = false;

    @Override//
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        //registering Sensor
        Sensor sensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        mSensorManager.registerListener(this,
                mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_UI);

        //then you should return sticky
        return Service.START_STICKY;
    }
    @Override
    public void onCreate() {
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        //add this line only
    }
    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }


    @Override
    public void onSensorChanged(SensorEvent se) {
        // get sensor when Change
        //Toast.makeText(getApplicationContext(), "Detected", Toast.LENGTH_SHORT).show();


        final float alpha = 0.8f;

        double x = se.values[0];
        double y = se.values[1];
        double z = se.values[2];

        if(x > 7){
            pointsPositive[0] = pointsPositive[1];
            pointsPositive[1] = pointsPositive[2];
            pointsPositive[2] = x;
            countPointsPositive++;
            if(countPointsPositive >= 2){
                countPointsPositive = 0;
            }

            if (pointsPositive[1] > pointsPositive[0] && pointsPositive[1] > pointsPositive[2]){
                isPositive = true;
            }
        }else if(x < -10){

            pointsNegative[0] = pointsNegative[1];
            pointsNegative[1] = pointsNegative[2];
            pointsNegative[2] = x;
            countPointsNegative++;
            if (countPointsNegative >= 2){
                countPointsNegative = 0;
            }

            if (pointsNegative[1] > pointsNegative[0] && pointsNegative[1] > pointsNegative[2] && isPositive){
                isNegative = true;
            }
        }


        if(isNegative){

            Toast.makeText(this, "send data", Toast.LENGTH_SHORT).show();
            Class activity = SendDataActivity.class;
            Intent intent = new Intent(this, activity);
            startActivity(intent);




            isNegative = false;
            isPositive = false;
        }
        //Log.v("AxisX", "x: " + x);
        //Log.v("AxisY", "x: " + y);
        //Log.v("AxisZ", "x: " + z);


        if (this.onGetPoints != null){
            this.onGetPoints.onGetPoitns(x,y,z);
        }else{
            String data = "";
        }


    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }


}
