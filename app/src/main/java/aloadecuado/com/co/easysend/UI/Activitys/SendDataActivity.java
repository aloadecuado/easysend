package aloadecuado.com.co.easysend.UI.Activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

import aloadecuado.com.co.easysend.HELPERS.ExternalStorage;
import aloadecuado.com.co.easysend.MODELS.UserObject;
import aloadecuado.com.co.easysend.NETWORK.Bl.GetUserClass;
import aloadecuado.com.co.easysend.NETWORK.Bl.SetUserClass;
import aloadecuado.com.co.easysend.UTIL.WriteReadStorageClass;

import static aloadecuado.com.co.easysend.UTIL.GlobalConstansAndVarClass.KEY_STORED_ID_USER;
import static aloadecuado.com.co.easysend.UTIL.GlobalConstansAndVarClass.KEY_STORED_LAST_PATH_IMAGE;
import static aloadecuado.com.co.easysend.UTIL.GlobalConstansAndVarClass.USEROBJECT;

public class SendDataActivity extends AppCompatActivity {

    private static Socket s;
    //private static ServerSocket ss;
    private static InputStreamReader isr;
    private static BufferedReader br;
    private static PrintWriter printWriter;

    String messagge = "";

    byte[] byteArraySend;
    private String ip = "10.25.64.174";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_send_data);

        ExternalStorage externalStorage = new ExternalStorage();
        ArrayList<String> pathsData = externalStorage.getAllShownImagesPath(this);

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap;

        WriteReadStorageClass writeReadStorageClass = new WriteReadStorageClass();
        String lastPathImage = writeReadStorageClass.getDataString(this,KEY_STORED_LAST_PATH_IMAGE);
        String printDta = pathsData.get(pathsData.size() - 1);
        /*while(lastPathImage.equals(printDta)){
            pathsData = externalStorage.getAllShownImagesPath(this);
            printDta = pathsData.get(pathsData.size() - 1);

        }*/

        writeReadStorageClass.setDataString(this,pathsData.get(pathsData.size() - 1), KEY_STORED_LAST_PATH_IMAGE );
        bitmap = BitmapFactory.decodeFile(pathsData.get(pathsData.size() - 1),bmOptions);
        /*ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        byteArraySend = byteArray;
        bitmap.recycle();*/


        String id = new WriteReadStorageClass().getDataString(this, KEY_STORED_ID_USER);
        new GetUserClass().getUserForId(this, id, new GetUserClass.onGetUser() {
            @Override
            public void onGetUser(UserObject userObjects) {
                USEROBJECT = userObjects;
                new SetUserClass().addBitmapUserForId(bitmap, USEROBJECT, new SetUserClass.onGetUser() {
                    @Override
                    public void onGetUser(UserObject userObject) {
                        USEROBJECT = userObject;
                        SendDataActivity.this.finish();
                    }

                    @Override
                    public void onGetUrl(String url) {
                        messagge = url;
                        MyTask mt = new MyTask();
                        mt.execute();
                    }
                });
            }
        });

    }

    class MyTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {

            try{
                s = new Socket(ip, 9001);
                printWriter = new PrintWriter(s.getOutputStream());
                printWriter.write(messagge);
                printWriter.flush();
                printWriter.close();
                s.close();
            }catch (Exception e){
                //Toast.makeText(ACTIVITY, "error: " + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }

            return null;
        }
    }
}
