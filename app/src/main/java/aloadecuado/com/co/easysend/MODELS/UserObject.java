package aloadecuado.com.co.easysend.MODELS;

import java.util.List;

/**
 * Created by Pedro on 29/01/2018.
 */

public class UserObject {

    private String id = "";
    private String uid = "";
    private String name = "";
    private String nick = "";
    private String dateRegister = "";
    private String idNumber = "";
    private List<String> urlsDocuments = null;
    private String email = "";

    private List<ImageObject> imageObjects = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getDateRegister() {
        return dateRegister;
    }

    public void setDateRegister(String dateRegister) {
        this.dateRegister = dateRegister;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public List<String> getUrlsDocuments() {
        return urlsDocuments;
    }

    public void setUrlsDocuments(List<String> urlsDocuments) {
        this.urlsDocuments = urlsDocuments;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<ImageObject> getImageObjects() {
        return imageObjects;
    }

    public void setImageObjects(List<ImageObject> imageObjects) {
        this.imageObjects = imageObjects;
    }
}
