package aloadecuado.com.co.easysend.UI.Activitys;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.hardware.SensorEvent;

import android.os.Build;
import android.os.Bundle;

import android.util.Log;

import android.view.View;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import aloadecuado.com.co.easysend.MODELS.UserObject;
import aloadecuado.com.co.easysend.NETWORK.Bl.SetUserClass;
import aloadecuado.com.co.easysend.R;
import aloadecuado.com.co.easysend.UTIL.WriteReadStorageClass;
import aloadecuado.com.co.easysend.SERVICES.GetCordinatesAcelerometer;
import aloadecuado.com.co.graphview.GraphView;
import aloadecuado.com.co.graphview.Model.GraphObject;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static aloadecuado.com.co.easysend.UTIL.GlobalConstansAndVarClass.KEY_STORED_ID_USER;
import static aloadecuado.com.co.easysend.UTIL.GlobalConstansAndVarClass.USEROBJECT;


public class MainActivity extends AppCompatActivity implements GetCordinatesAcelerometer.OnGetPoints {

    // Create a constant to convert nanoseconds to seconds.


    public static GetCordinatesAcelerometer.OnGetPoints ONGETPOINTS;
    public static Activity ACTIVITY;
    private GraphView gvAccelerometer;


    private static final String TAG = "MediaProjectionDemo";
    private static final int PERMISSION_CODE = 1;


    private static final int RC_SIGN_IN = 123;


    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 2020;

    // The request code used in ActivityCompat.requestPermissions()
// and returned in the Activity's onRequestPermissionsResult()
    int PERMISSION_ALL = 1;
    String[] PERMISSIONS = {
            android.Manifest.permission.CAMERA,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.INTERNET
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        ONGETPOINTS = this;

        gvAccelerometer = (GraphView) findViewById(R.id.gvAccelerometer);


        gvAccelerometer.setMaxValue(200);
        GraphObject graphObject1 = new GraphObject(0, Color.RED, 2, 0);
        GraphObject graphObject2 = new GraphObject(1, Color.BLUE, 2, 0);
        GraphObject graphObject3 = new GraphObject(2, Color.GREEN, 2, 0);
        //gvAccelerometer.addGraph(graphObject1);
        //gvAccelerometer.addGraph(graphObject2);
        //gvAccelerometer.addGraph(graphObject3);
        // use this to start and trigger a service
        Intent i= new Intent(this, GetCordinatesAcelerometer.class);
// potentially add data to the intent
        //i.putExtra("KEY1", "Value to be used by the service");
        this.startService(i);



        ACTIVITY = this;
        buildDataStorgae();

        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }
        //sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        //sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        /*takeScreenShot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Bitmap b = new Utils().takeScreenShot(MainActivity.this);//takescreenshotOfRootView(gvAccelerometer);
                        screenShotImageView.setImageBitmap(b);

                        isTake = true;
                    }
                }, 5000);


            }
        });*/




        //startActivity(new Intent(this, LoadImagesGalleryActivity.class));
        //this.finish();


    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @OnClick(R.id.btScanQr)
    public void scanQr(View view){
        startActivity(new Intent(this, ScanQRActivity.class));

    }
    public void buildDataStorgae(){
        final FirebaseAuth auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() != null) {


            floidNavigation(auth);

        } else {


            List<AuthUI.IdpConfig> providers = Arrays.asList(
                    new AuthUI.IdpConfig.EmailBuilder().build(),
                    new AuthUI.IdpConfig.GoogleBuilder().build());

            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(providers)
                            .setIsSmartLockEnabled(false)
                            .build(),
                    RC_SIGN_IN);
        }
    }

    private void floidNavigation(final FirebaseAuth auth)
    {
        FirebaseUser user = auth.getCurrentUser();
        UserObject userObject = new UserObject();


        userObject.setEmail(user.getEmail());
        userObject.setName(user.getDisplayName());
        userObject.setUid(user.getUid());

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        Date convertedDate = new Date();

        String strDate = dateFormat.format(convertedDate);
        userObject.setDateRegister(strDate);
        new SetUserClass().createUserKeyForUid(MainActivity.this,userObject, new SetUserClass.onGetUser() {
            @Override
            public void onGetUser(UserObject userObject) {
                USEROBJECT = userObject;

                new WriteReadStorageClass().setDataString(MainActivity.this, USEROBJECT.getId(), KEY_STORED_ID_USER);


            }

            @Override
            public void onGetUrl(String url) {

            }
        });
    }
    public String returnDateFormat(int item){
        String dato = "" + item;
        if (dato.length() <= 1){
            return "0" + dato;
        }

        return dato;
    }
    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // RC_SIGN_IN is the request code you passed into startActivityForResult(...) when starting the sign in flow.
        if (requestCode == RC_SIGN_IN) {



            // Successfully signed in
            if (resultCode == RESULT_OK) {

                final FirebaseAuth auth = FirebaseAuth.getInstance();
                floidNavigation(auth);



                return;
            }


        }

    }


    public static Bitmap takescreenshot(View v) {
        v.setDrawingCacheEnabled(true);
        v.buildDrawingCache(true);
        Bitmap b = loadLargeBitmapFromView(v);//Bitmap.createBitmap(v.getDrawingCache());
        v.setDrawingCacheEnabled(false);
        return b;
    }

    public static Bitmap loadLargeBitmapFromView(View v)
    {
        Bitmap b = Bitmap.createBitmap(v.getWidth(), v.getHeight(), Bitmap.Config.ARGB_8888);

        Canvas c = new Canvas(b);
        v.layout(0, 0, v.getLayoutParams().width, v.getLayoutParams().height);
        v.draw(c);
        return b;
    }

    public static Bitmap takescreenshotOfRootView(View v) {
        return takescreenshot(v.getRootView());
    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Bitmap b = takescreenshotOfRootView(gvAccelerometer);
        //screenShotImageView.setImageBitmap(b);
    }

    public void onSensorChanged(SensorEvent event){
        // In this example, alpha is calculated as t / (t + dT),
        // where t is the low-pass filter's time-constant and
        // dT is the event delivery rate.

        final float alpha = 0.8f;

        double x = event.values[0];
        double y = event.values[1];
        double z = event.values[2];

        //gvAccelerometer.setPointGraph(0, (int)x);
        //gvAccelerometer.setPointGraph(1, (int)y);
        //gvAccelerometer.setPointGraph(2, (int)z);

        Log.v("AxisX", "x: " + x);
        Log.v("AxisY", "x: " + y);
        Log.v("AxisZ", "x: " + z);



        //gravity[1] = alpha * gravity[1] + (1 - alpha) * event.values[1];
        //gravity[2] = alpha * gravity[2] + (1 - alpha) * event.values[2];
        // Isolate the force of gravity with the low-pass filter.
        //gravity[0] = alpha * gravity[0] + (1 - alpha) * event.values[0];
        //gravity[1] = alpha * gravity[1] + (1 - alpha) * event.values[1];
        //gravity[2] = alpha * gravity[2] + (1 - alpha) * event.values[2];

        // Remove the gravity contribution with the high-pass filter.
        //linear_acceleration[0] = event.values[0] - gravity[0];
        //linear_acceleration[1] = event.values[1] - gravity[1];
        //linear_acceleration[2] = event.values[2] - gravity[2];
    }


    @Override
    public void onGetPoitns(double x, double y, double z) {
        //gvAccelerometer.setPointGraph(0, (int)x);
        //gvAccelerometer.setPointGraph(1, (int)y);
        //gvAccelerometer.setPointGraph(2, (int)z);
        //gvAccelerometer.addDataPointx((float)(x + y + z));

            gvAccelerometer.addDataPointx((float)x);

            double delta = x;

        //gvAccelerometer.addDataPointy((float)y);
        //gvAccelerometer.addDataPointz((float)z);
    }

    @Override
    public void onGetIsMovet() {

    }

    /*private void takeScreenshot() {
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        try {
            // image naming and path  to include sd card  appending name you choose for file
            String mPath = Environment.getExternalStorageDirectory().toString() + "/" + now + ".jpg";

            // create bitmap screen capture
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = loadLargeBitmapFromView(v1);//Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

            openScreenshot(imageFile);
        } catch (Throwable e) {
            // Several error may come out with file handling or DOM
            e.printStackTrace();
        }
    }

    private void openScreenshot(File imageFile) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(imageFile);
        intent.setDataAndType(uri, "image/*");
        startActivity(intent);
    }

    private static class Resolution {
        int x;
        int y;

        public Resolution(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public String toString() {
            return x + "x" + y;
        }
    }*/
}
