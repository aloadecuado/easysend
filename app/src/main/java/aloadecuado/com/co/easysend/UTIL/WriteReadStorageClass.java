package aloadecuado.com.co.easysend.UTIL;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by pedrodaza on 31/01/18.
 */

public class WriteReadStorageClass {

    public void setDataString(Activity context, String string, String key)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences("aloadecuado", MODE_PRIVATE);
        //SharedPreferences sharedPref = context.getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, string);
        editor.commit();


    }

    public String getDataString(Activity activity, String key)
    {
        SharedPreferences sharedPreferences = activity.getSharedPreferences("aloadecuado", MODE_PRIVATE);

        String App = sharedPreferences.getString(key, "Sin asignar");

        return App;
    }
}
