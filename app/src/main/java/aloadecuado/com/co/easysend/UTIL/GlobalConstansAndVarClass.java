package aloadecuado.com.co.easysend.UTIL;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import aloadecuado.com.co.easysend.MODELS.OrderLinked;
import aloadecuado.com.co.easysend.MODELS.UserObject;


/**
 * Created by Pedro on 30/12/2017.
 */

public class GlobalConstansAndVarClass {
    //firebase url
    public static String FIREURL_USERS = "Users";
    public static String FIREURL_ORDERS_LINKED = "OrdersLinked";


    //Save key storage

    public static String KEY_STORED_ID_USER = "KEY_STORED_ID_USER";
    public static String KEY_STORED_LAST_PATH_IMAGE = "KEY_STORED_LAST_PATH_IMAGE";



    //KeyS firebase
    public static String FIREKEY_USER = "";
    public static String FIREKEY_USERUID = "";

    // Object

    public static UserObject USEROBJECT;
    public static OrderLinked ORDERLINKED;





}
